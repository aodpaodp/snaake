#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"

#define OLC_PGEX_SOUND
#include "olcPGEX_Sound.h"

#include <iostream>
#include <math.h>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

class Snake : public olc::PixelGameEngine
{
public:
	Snake()
	{
		sAppName = "Snake";
	}

private:
	int mapWidth;
	int mapHeight;
	int screenPosX;
	int screenPosY;
	int tileSize = 32;
	int headPosX;
	int headPosY;
	int headPosX2;
	int headPosY2;
	int velX = 0;
	int velY = 0;
	int velX2 = 0;
	int velY2 = 0;
	int fruitPosX;
	int fruitPosY;
	int score;
	int score2;
	vector<pair<int, int>> segments;
	vector<pair<int, int>> segments2;
	int length = 0;
	int length2 = 0;
	bool gameOver;
	float speed = 0.1f; // maxSpeed = 0
	float time;
	bool language = 1;
	bool sound = 1;
	bool music = 1;
	vector<int> highscore = { 0,0,0,0 };
	bool lost_save = true;

	olc::Sprite* menuSprite = nullptr;
	int layerMenu = 0;
	olc::Sprite* grass2Sprite = nullptr;
	olc::Decal* grass2Decal = nullptr;
	int layerMap = 0;
	olc::Sprite* night = nullptr;
	olc::Decal* nightDecal = nullptr;
	int layerNight = 0;
	olc::Sprite* headSprite = nullptr;
	olc::Decal* headDecal = nullptr;
	olc::Sprite* bodySprite = nullptr;
	olc::Decal* bodyDecal = nullptr;
	olc::Sprite* wallSprite = nullptr;
	olc::Decal* wallDecal = nullptr;
	int faded;
	int eat;
	int move;
	int die;
	int click;
	int over;

	float storyTime = 0.0f;
	float spawnTime = 0.0f;
	int storyMapSize = 100;
	int storyMap[100][100] = { 0 };
	float cameraPosX = 0.0f;
	float cameraPosY = 0.0f;
	float snakePosX = (float)storyMapSize / 2;
	float snakePosY = (float)storyMapSize / 2;
	float sVelX = 0.0f;
	float sVelY = 0.0f;
	int snakeLevel = 0;
	float invincible = 0.0f;
	int nDir = 0;
	olc::Sprite* houseSprite = nullptr;
	olc::Decal* houseDecal = nullptr;
	olc::Sprite* treeSprite = nullptr;
	olc::Decal* treeDecal = nullptr;
	olc::Sprite* fruitSprite = nullptr;
	olc::Decal* fruitDecal = nullptr;
	olc::Sprite* sprSpawner[2];
	olc::Decal* decSpawner[2];
	olc::Sprite* animalSprite[3];
	olc::Decal* animalDecal[3];
	olc::Sprite* sprDawn;
	olc::Decal* decDawn;

	struct animal;

	struct animalDefinition
	{
		uint32_t spriteID = 0;
		int strength = 0;
		bool dead = false;
		function<void(animal&, float)> funcMove;
	};

	struct animal
	{
		olc::vf2d pos;
		olc::vf2d vel;
		animalDefinition def;
		float movePattern = 0.0f;

		void Update(float fElapsedTime)
		{
			def.funcMove(*this, fElapsedTime);
		}
	};

	struct spawner
	{
		olc::vi2d pos;
		int spawnType;
	};

	struct fruit
	{
		olc::vi2d pos;
		float regenTime = 0.0f;
		bool avail = true;

	};

	list<animalDefinition> listSpawns;
	list<animal> listAnimals;
	list<animalDefinition> listImmDef;
	list<animal> listImmortals;
	list<fruit> listFruits;
	vector<spawner> listSpawners;

	enum class GS
	{
		MAINMENU,
		PLAYGAME,
		CONTINUE,
		HIGHSCORE,
		STORY,
		CLASSIC,
		MODERN,
		NIGHT,
		SETTINGS,
		TWOPLAYERS
	} GameState = GS::MAINMENU;

	void resetSnake()
	{
		segments.clear();
		segments2.clear();
		velX = 0;
		velY = 0;
		velX2 = 0;
		velY2 = 0;
		length = 0;
		length2 = 0;
		score = 0;
		score2 = 0;
		headPosX = mapWidth / 2;
		headPosY = mapHeight / 2;
		headPosX2 = screenPosX;
		headPosY2 = screenPosY;
		lost_save = true;
	}

	void drawString(int a, int b, string english, string vietnamese, olc::Pixel col = olc::WHITE, int c = 1) {
		if (language) {
			//DrawStringDecal(olc::vi2d(a, b), english, col, olc::vi2d(c,c));
			DrawString(a, b, english, col, c);
		}
		else {
			//DrawStringDecal(olc::vi2d(a, b), vietnamese, col, olc::vi2d(c,c));
			DrawString(a, b, vietnamese, col, c);
		}
	}

	void playMusic(int a) {
		if (music) {
			olc::SOUND::StopSample(a);
			music = false;
		}
		else {
			olc::SOUND::PlaySample(a, true);
			music = true;
		}
	}

	void playSound(int a) {
		if (sound) {
			olc::SOUND::PlaySample(a);
		}
	}

public:
	bool OnUserCreate() override
	{
		auto GetTile = [&](int x, int y)
		{
			if (x >= 0 && x < storyMapSize && y >= 0 && y < storyMapSize)
				return storyMap[x][y];
			else
				return -1;
		};

		mapWidth = 20;
		mapHeight = 20;
		headPosX = mapWidth / 2;
		headPosY = mapHeight / 2;
		headPosX2 = screenPosX;
		headPosY2 = screenPosY;
		fruitPosX = rand() % mapWidth;
		fruitPosY = rand() % mapHeight;
		score = 0;
		score2 = 0;
		gameOver = 0;

		screenPosX = (ScreenWidth() - mapWidth * tileSize) / 2;
		screenPosY = (ScreenHeight() - mapHeight * tileSize) / 2;

		ifstream objectPos("objectPos.txt");
		if (objectPos.fail())
		{
			cerr << "Unable to open file objectPos.txt";
			_Exit(0);
		}
		while (!objectPos.eof())
		{
			int x, y, v;
			char value;
			objectPos >> x >> y >> value;
			v = value;
			storyMap[x][y] = v;
			if (v == 't')
			{
				fruit f1 = { olc::vi2d(x, y + 3) };
				fruit f2 = { olc::vi2d(x + 2, y + 3) };
				listFruits.push_back(f1);
				listFruits.push_back(f2);
			}
			else if (v == 'c')
			{
				spawner s = { olc::vi2d(x,y), 8 };
				listSpawners.push_back(s);
			}
			else if (v == 'b')
			{
				spawner s = { olc::vi2d(x,y), 4 };
				listSpawners.push_back(s);
			}

		}
		objectPos.close();

		grass2Sprite = new olc::Sprite("Assets/Art/ingame/grass2.png");
		grass2Decal = new olc::Decal(grass2Sprite);
		layerMap = CreateLayer();
		SetDrawTarget(layerMap);
		DrawSprite({ screenPosX, screenPosY }, grass2Sprite);

		menuSprite = new olc::Sprite("Assets/Art/background/background1280x720.png");
		layerMenu = CreateLayer();
		SetDrawTarget(layerMenu);
		DrawSprite({ 0, 0 }, menuSprite);
		EnableLayer(layerMenu, true);

		SetDrawTarget(nullptr);

		headSprite = new olc::Sprite("Assets/Art/ingame/SnakeHead.png");
		headDecal = new olc::Decal(headSprite);
		bodySprite = new olc::Sprite("Assets/Art/ingame/SnakeBody.png");
		bodyDecal = new olc::Decal(bodySprite);
		fruitSprite = new olc::Sprite("Assets/Art/ingame/orange.png");
		fruitDecal = new olc::Decal(fruitSprite);
		wallSprite = new olc::Sprite("Assets/Art/ingame/bricks.png");
		wallDecal = new olc::Decal(wallSprite);
		night = new olc::Sprite("Assets/Art/ingame/night.png");
		nightDecal = new olc::Decal(night);

		houseSprite = new olc::Sprite("Assets/Art/ingame/house.png");
		houseDecal = new olc::Decal(houseSprite);
		treeSprite = new olc::Sprite("Assets/Art/ingame/tree.png");
		treeDecal = new olc::Decal(treeSprite);
		sprSpawner[0] = new olc::Sprite("Assets/Art/ingame/cave.png");
		decSpawner[0] = new olc::Decal(sprSpawner[0]);
		sprSpawner[1] = new olc::Sprite("Assets/Art/ingame/burrow.png");
		decSpawner[1] = new olc::Decal(sprSpawner[1]);
		animalSprite[0] = new olc::Sprite("Assets/Art/ingame/bear.png");
		animalDecal[0] = new olc::Decal(animalSprite[0]);
		animalSprite[1] = new olc::Sprite("Assets/Art/ingame/bunny.png");
		animalDecal[1] = new olc::Decal(animalSprite[1]);
		animalSprite[2] = new olc::Sprite("Assets/Art/ingame/immortal.png");
		animalDecal[2] = new olc::Decal(animalSprite[2]);
		sprDawn = new olc::Sprite("Assets/Art/background/dawn.png");
		decDawn = new olc::Decal(sprDawn);


		// Movement for animal
		auto Move_None = [&](animal& a, float fElapsedTime)
		{

		};

		auto Move_Bear = [&](animal& a, float fElapsedTime)
		{
			constexpr float fDelay = 1.0f;
			a.movePattern += fElapsedTime;
			if (a.movePattern >= fDelay)
			{
				switch (rand() % 4)
				{
				case 0:
					a.vel = { 6.0f, 0.0f };
					break;
				case 1:
					a.vel = { -6.0f, 0.0f };
					break;
				case 2:
					a.vel = { 0.0f, 6.0f };
					break;
				case 3:
					a.vel = { 0.0f, -6.0f };
					break;
				}
				a.movePattern -= fDelay;
			}
			float newPosX = a.pos.x + a.vel.x * fElapsedTime;
			float newPosY = a.pos.y + a.vel.y * fElapsedTime;
			if (a.pos.x < 0)
			{
				a.pos.x = 0;
				a.vel.x = -a.vel.x;
			}
			if (a.pos.x >= storyMapSize)
			{
				a.pos.y = (float)storyMapSize;
				a.vel.x = -a.vel.x;
			}
			if (a.pos.y < 0)
			{
				a.pos.y = 0;
				a.vel.y = -a.vel.y;
			}
			if (a.pos.y >= storyMapSize)
			{
				a.pos.y = (float)storyMapSize;
				a.vel.y = -a.vel.y;
			}
			a.pos += a.vel * fElapsedTime;
		};

		auto Move_Rabbit = [&](animal& a, float fElapsedTime)
		{
			constexpr float fDelay = 2.0f;
			a.movePattern += fElapsedTime;
			if (a.movePattern >= fDelay)
			{
				switch (rand() % 5)
				{
				case 0:
					a.vel = { 4.0f, 0.0f };
					break;
				case 1:
					a.vel = { -4.0f, 0.0f };
					break;
				case 2:
					a.vel = { 0.0f, 4.0f };
					break;
				case 3:
					a.vel = { 0.0f, -4.0f };
					break;
				case 4:
					a.vel = { 0.0f, 0.0f };
					break;
				}
				a.movePattern -= fDelay;
			}
			float newPosX = a.pos.x + a.vel.x * fElapsedTime;
			float newPosY = a.pos.y + a.vel.y * fElapsedTime;
			if (a.pos.x < 0)
			{
				a.pos.x = 0;
				a.vel.x = -a.vel.x;
			}
			if (a.pos.x >= storyMapSize)
			{
				a.pos.y = (float)storyMapSize;
				a.vel.x = -a.vel.x;
			}
			if (a.pos.y < 0)
			{
				a.pos.y = 0;
				a.vel.y = -a.vel.y;
			}
			if (a.pos.y >= storyMapSize)
			{
				a.pos.y = (float)storyMapSize;
				a.vel.y = -a.vel.y;
			}
			a.pos += a.vel * fElapsedTime;
		};

		auto Move_Immortal = [&](animal& a, float fElapsedTime)
		{
			constexpr float fDelay = 0.5f;
			a.movePattern += fElapsedTime;
			if (a.movePattern >= fDelay)
			{
				switch (rand() % 4)
				{
				case 0:
					a.vel = { 10.0f, 0.0f };
					break;
				case 1:
					a.vel = { -10.0f, 0.0f };
					break;
				case 2:
					a.vel = { 0.0f, 10.0f };
					break;
				case 3:
					a.vel = { 0.0f, -10.0f };
					break;
				}
				a.movePattern -= fDelay;
			}
			float newPosX = a.pos.x + a.vel.x * fElapsedTime;
			float newPosY = a.pos.y + a.vel.y * fElapsedTime;
			if (newPosX <= 53.0f && newPosX >= 48.0f && newPosY <= 53.0f && newPosY >= 48.0f)
			{
				a.vel.x = -a.vel.x;
				a.vel.y = -a.vel.y;
			}
			a.pos += a.vel * fElapsedTime;
		};

		listSpawns =
		{
			{1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit},
			{1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit},
			{1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit},
			{1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit},
			{1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit},
			{1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit},
			{1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit},
			{1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit}, {1, 4, 0, Move_Rabbit},
			{0, 8, 0, Move_None}, {0, 8, 0, Move_Bear}, {0, 8, 0, Move_Bear},
			{0, 8, 0, Move_None}, {0, 8, 0, Move_Bear}, {0, 8, 0, Move_Bear},
			{0, 8, 0, Move_None}, {0, 8, 0, Move_Bear}, {0, 8, 0, Move_Bear},
			{0, 8, 0, Move_None}, {0, 8, 0, Move_Bear}, {0, 8, 0, Move_Bear},
		};
		listImmDef =
		{
			{2, 999, 0, Move_Immortal}, {2, 999, 0, Move_Immortal}, {2, 999, 0, Move_Immortal}, {2, 999, 0, Move_Immortal},
			{2, 999, 0, Move_Immortal}, {2, 999, 0, Move_Immortal}, {2, 999, 0, Move_Immortal}, {2, 999, 0, Move_Immortal},
		};

		loadSettings();

		olc::SOUND::InitialiseAudio(44100, 1, 8, 512);
		faded = olc::SOUND::LoadAudioSample("Assets/Music/faded.wav");
		eat = olc::SOUND::LoadAudioSample("Assets/Music/SnakeEat.wav");
		move = olc::SOUND::LoadAudioSample("Assets/Music/SnakeMove.wav");
		die = olc::SOUND::LoadAudioSample("Assets/Music/SnakeDie.wav");
		over = olc::SOUND::LoadAudioSample("Assets/Music/ButtonOver.wav");
		click = olc::SOUND::LoadAudioSample("Assets/Music/ButtonClick.wav");
		olc::SOUND::PlaySample(faded, true);

		checkToCreateHighscore();
		loadHighscore();


		return true;
	}

	int mainSelection = 0;
	bool MainMenu()
	{
		Clear(olc::BLANK);
		drawString(350, 50, "Snake Project", "Ran san moi", olc::WHITE, 5);
		drawString(350, 200, "New game", "Bat dau", mainSelection == 0 ? olc::YELLOW : olc::WHITE, 5);
		drawString(350, 300, "Continue", "Tiep tuc", mainSelection == 1 ? olc::YELLOW : olc::WHITE, 5);
		drawString(350, 400, "Highscore", "Bang xep hang", mainSelection == 2 ? olc::YELLOW : olc::WHITE, 5);
		drawString(350, 500, "Settings", "Cai dat", mainSelection == 3 ? olc::YELLOW : olc::WHITE, 5);
		drawString(350, 600, "Exit", "Thoat", mainSelection == 4 ? olc::YELLOW : olc::WHITE, 5);
		if (IsFocused())
		{
			if (GetKey(olc::UP).bPressed || GetKey(olc::W).bPressed)
			{
				playSound(over);
				mainSelection--;
				if (mainSelection < 0) mainSelection = 4;
			}
			if (GetKey(olc::DOWN).bPressed || GetKey(olc::S).bPressed)
			{
				playSound(over);
				mainSelection++;
				if (mainSelection > 4) mainSelection = 0;
			}
			if (GetKey(olc::SPACE).bPressed || GetKey(olc::ENTER).bPressed)
			{
				playSound(click);
				if (mainSelection == 0) GameState = GS::PLAYGAME;
				if (mainSelection == 1) GameState = GS::CONTINUE;
				if (mainSelection == 2) GameState = GS::HIGHSCORE;
				if (mainSelection == 3) GameState = GS::SETTINGS;
				if (mainSelection == 4) olc_Terminate();
				//Clear(olc::BLANK);
			}
		}
		return true;
	}

	int gameSelection = 0;
	bool PlayGame()
	{
		Clear(olc::BLANK);
		drawString(300, 50, "Start a new game ~", "Bat dau", olc::WHITE, 5);
		drawString(300, 100, "Story", "Cot truyen", gameSelection == 0 ? olc::YELLOW : olc::WHITE, 5);
		drawString(300, 200, "Classic", "Kinh dien", gameSelection == 1 ? olc::YELLOW : olc::WHITE, 5);
		drawString(300, 300, "Modern", "Hien dai", gameSelection == 2 ? olc::YELLOW : olc::WHITE, 5);
		drawString(300, 400, "Night", "Ban dem", gameSelection == 3 ? olc::YELLOW : olc::WHITE, 5);
		drawString(300, 500, "Two Players", "Hai nguoi choi", gameSelection == 4 ? olc::YELLOW : olc::WHITE, 5);
		drawString(300, 600, "Back", "Tro lai", gameSelection == 5 ? olc::YELLOW : olc::WHITE, 5);

		if (IsFocused())
		{
			if (GetKey(olc::UP).bPressed || GetKey(olc::W).bPressed)
			{
				playSound(over);
				gameSelection--;
				if (gameSelection < 0) gameSelection = 5;
			}
			if (GetKey(olc::DOWN).bPressed || GetKey(olc::S).bPressed)
			{
				playSound(over);
				gameSelection++;
				if (gameSelection > 5) gameSelection = 0;
			}
			if (GetKey(olc::SPACE).bPressed || GetKey(olc::ENTER).bPressed)
			{
				playSound(click);
				if (gameSelection == 0) GameState = GS::STORY;
				if (gameSelection == 1) GameState = GS::CLASSIC;
				if (gameSelection == 2) GameState = GS::MODERN;
				if (gameSelection == 3) GameState = GS::NIGHT;
				if (gameSelection == 4) GameState = GS::TWOPLAYERS;
				if (gameSelection == 5) GameState = GS::MAINMENU;
				Clear(olc::BLANK);
			}
		}
		return true;
	}

	int continueSelection = 0;
	bool ContinueGame()
	{
		Clear(olc::BLANK);
		drawString(300, 50, "Choose mode to continue", "Chon che do tiep tuc", olc::WHITE, 5);
		drawString(300, 170, "Story", "Cot truyen", continueSelection == 0 ? olc::YELLOW : olc::WHITE, 5);
		drawString(300, 240, "Normal", "Binh thuong", continueSelection == 1 ? olc::YELLOW : olc::WHITE, 5);
		drawString(300, 500, "Back", "Tro lai", continueSelection == 2 ? olc::YELLOW : olc::WHITE, 5);

		if (IsFocused())
		{
			if (GetKey(olc::UP).bPressed || GetKey(olc::W).bPressed)
			{
				playSound(over);
				continueSelection--;
				if (continueSelection < 0) continueSelection = 2;
			}
			if (GetKey(olc::DOWN).bPressed || GetKey(olc::S).bPressed)
			{
				playSound(over);
				continueSelection++;
				if (continueSelection > 2) continueSelection = 0;
			}
			if (GetKey(olc::SPACE).bPressed || GetKey(olc::ENTER).bPressed)
			{
				playSound(click);
				if (continueSelection == 0)
				{
					loadStory();
					GameState = GS::STORY;
				}
				if (continueSelection == 1)
				{
					switch (loadGame())
					{
					case 0:
						GameState = GS::PLAYGAME;
						break;
					case 1:
						GameState = GS::CLASSIC;
						break;
					case 2:
						GameState = GS::MODERN;
						break;
					case 3:
						GameState = GS::NIGHT;
						break;
					default:
						break;
					}
				}
				if (continueSelection == 2) GameState = GS::MAINMENU;
				Clear(olc::BLANK);
			}
		}
		return 1;
	}

	bool Highscore()
	{
		loadHighscore();
		Clear(olc::BLANK);
		string s0 = "Story      Level : ",
			s1 = "Classic    Score : ",
			s2 = "Modern     Score : ",
			s3 = "Night      Score : ",
			s4 = "Cot truyen   Cap : ",
			s5 = "Kinh dien   Diem : ",
			s6 = "Hien dai    Diem : ",
			s7 = "Ban dem     Diem : ";
		s0 += highscore[0] + 48;
		s1 += highscore[1] + 48;
		s2 += highscore[2] + 48;
		s3 += highscore[3] + 48;
		s4 += highscore[0] + 48;
		s5 += highscore[1] + 48;
		s6 += highscore[2] + 48;
		s7 += highscore[3] + 48;

		drawString(300, 50, "High Score", "Diem cao", olc::WHITE, 5);
		drawString(200, 200, s0, s4, olc::WHITE, 5);
		drawString(200, 250, s1, s5, olc::WHITE, 5);
		drawString(200, 300, s2, s6, olc::WHITE, 5);
		drawString(200, 350, s3, s7, olc::WHITE, 5);
		drawString(200, 600, "Back", "Tro lai", olc::YELLOW, 5);

		if (GetKey(olc::SPACE).bPressed || GetKey(olc::ENTER).bPressed)
		{
			playSound(click); GameState = GS::MAINMENU;
			Clear(olc::BLANK);
		}
		return true;
	}

	bool pause = false;
	bool exit = false;
	int state;
	bool TwoPlayers(float fElapsedTime) {
		if (IsFocused())
		{
			if (velY == 0 && GetKey(olc::UP).bPressed && !pause)
			{
				velX = 0;
				velY = -1;
			}
			if (velY == 0 && GetKey(olc::DOWN).bPressed && !pause)
			{
				velX = 0;
				velY = 1;
			}
			if (velX == 0 && GetKey(olc::LEFT).bPressed && !pause)
			{
				velX = -1;
				velY = 0;
			}
			if (velX == 0 && GetKey(olc::RIGHT).bPressed && !pause)
			{
				velX = 1;
				velY = 0;
			}

			if (velY2 == 0 && GetKey(olc::W).bPressed && !pause)
			{
				velX2 = 0;
				velY2 = -1;
			}
			if (velY2 == 0 && GetKey(olc::S).bPressed && !pause)
			{
				velX2 = 0;
				velY2 = 1;
			}
			if (velX2 == 0 && GetKey(olc::A).bPressed && !pause)
			{
				velX2 = -1;
				velY2 = 0;
			}
			if (velX2 == 0 && GetKey(olc::D).bPressed && !pause)
			{
				velX2 = 1;
				velY2 = 0;
			}
			if (GetKey(olc::ESCAPE).bPressed || GetKey(olc::P).bPressed)
			{
				pause = !pause;
			}
			if (pause)
			{
				drawString(350, 250, "EXIT?", "THOAT?", olc::WHITE, 5);
				drawString(450, 350, "YES", "CO", exit == true ? olc::YELLOW : olc::WHITE, 5);
				drawString(650, 350, "NO", "KHONG", exit == false ? olc::YELLOW : olc::WHITE, 5);
				if (GetKey(olc::LEFT).bPressed || GetKey(olc::A).bPressed || GetKey(olc::RIGHT).bPressed || GetKey(olc::D).bPressed)
				{
					playSound(over);
					exit = !exit;
				}
				if (GetKey(olc::ENTER).bPressed || GetKey(olc::SPACE).bPressed)
				{
					playSound(click);
					if (exit)
					{
						gameOver = true;
						gameOver = false;
						GameState = GS::MAINMENU;
						resetSnake();
						Clear(olc::BLANK);
					}
					pause = !pause;
				}
				return 1;
			}
			else
				time += fElapsedTime;
		}

		if (time >= speed)
		{
			Clear(olc::BLANK);
			for (auto i = length - 1; i > 0; i--)
			{
				auto prev = i - 1;
				segments[i].first = segments[prev].first;
				segments[i].second = segments[prev].second;
			}
			if (length)
			{
				segments[0].first = headPosX;
				segments[0].second = headPosY;
			}
			pair<int, int> prePos = make_pair(headPosX, headPosY);
			headPosX += velX;
			headPosY += velY;

			if (headPosY >= mapHeight) headPosY = 0;
			if (headPosY < 0) headPosY = (mapHeight - 1);
			if (headPosX >= mapWidth) headPosX = 0;
			if (headPosX < 0) headPosX = (mapWidth - 1);

			//player 2
			for (auto i = length2 - 1; i > 0; i--)
			{
				auto prev = i - 1;
				segments2[i].first = segments2[prev].first;
				segments2[i].second = segments2[prev].second;
			}
			if (length2)
			{
				segments2[0].first = headPosX2;
				segments2[0].second = headPosY2;
			}

			pair<int, int> prePos2 = make_pair(headPosX2, headPosY2);
			headPosX2 += velX2;
			headPosY2 += velY2;

			if (headPosY2 >= mapHeight) headPosY2 = 0;
			if (headPosY2 < 0) headPosY2 = (mapHeight - 1);
			if (headPosX2 >= mapWidth) headPosX2 = 0;
			if (headPosX2 < 0) headPosX2 = (mapWidth - 1);

			// Snake eat the fruit
			if (headPosX == fruitPosX && headPosY == fruitPosY)
			{
				score++;
				length++;
				fruitPosX = rand() % mapWidth;
				fruitPosY = rand() % mapHeight;
				segments.push_back(prePos);
				playSound(eat);
			}

			// Snake eat the fruit
			if (headPosX2 == fruitPosX && headPosY2 == fruitPosY)
			{
				score2++;
				length2++;
				fruitPosX = rand() % mapWidth;
				fruitPosY = rand() % mapHeight;
				segments2.push_back(prePos2);
				playSound(eat);
			}

			for (int i = 0; i < length; i++)
			{
				if (headPosX == segments[i].first && headPosY == segments[i].second) {
					gameOver = true;
					state = 2;
				}
				if (headPosX2 == segments[i].first && headPosY2 == segments[i].second) {
					gameOver = true;
					state = 1;
				}
			}

			for (int i = 0; i < length2; i++)
			{
				if (headPosX2 == segments2[i].first && headPosY2 == segments2[i].second) {
					gameOver = true;
					state = 1;
				}
				if (headPosX == segments2[i].first && headPosY == segments2[i].second) {
					gameOver = true;
					state = 2;
				}
			}

			if (headPosX == headPosX2 && headPosY == headPosY2) {
				gameOver = true;
				if (score == score2) state = 0;
				else if (score > score2) state = 1;
				else if (score < score2) state = 2;
			}

			drawString(screenPosY, screenPosY, "Score 1: " + to_string(length), "Diem 1:" + to_string(length), olc::WHITE, 3);
			drawString(screenPosY, screenPosY + 100, "Score 2: " + to_string(length2), "Diem 2:" + to_string(length2), olc::WHITE, 3);
			time = 0;
		}

		DrawDecal({ (float)fruitPosX * tileSize + screenPosX, (float)fruitPosY * tileSize + screenPosY }, fruitDecal);
		DrawDecal({ (float)headPosX2 * tileSize + screenPosX, (float)headPosY2 * tileSize + screenPosY }, headDecal, { 1.0f, 1.0f }, olc::Pixel(128, 64, 128));
		DrawDecal({ (float)headPosX * tileSize + screenPosX, (float)headPosY * tileSize + screenPosY }, headDecal);
		for (int i = 0; i < length; i++)
		{
			DrawDecal({ (float)segments[i].first * tileSize + screenPosX, (float)segments[i].second * tileSize + screenPosY }, bodyDecal);
		}

		for (int i = 0; i < length2; i++)
		{
			DrawDecal({ (float)segments2[i].first * tileSize + screenPosX, (float)segments2[i].second * tileSize + screenPosY }, bodyDecal, { 1.0f, 1.0f }, olc::Pixel(128, 64, 128));
		}

		if (gameOver)
		{
			playSound(die);
			if (state == 0) drawString(350, 150, "TIE", "HOA", olc::WHITE, 5);
			else if (state == 1) drawString(350, 150, "PLAYER 1 WON", "NGUOI CHOI 1 THANG", olc::WHITE, 5);
			else if (state == 2) drawString(350, 150, "PLAYER 2 WON", "NGUOI CHOI 2 THANG", olc::WHITE, 5);
			pause = !pause;
		}
		return true;
	}

	bool Story(float fElapsedTime)
	{

		if (gameOver)
		{
			DrawString(450, 300, "Game Over", olc::WHITE, 5);
			if (GetKey(olc::ENTER).bPressed)
			{
				saveHighscore(snakeLevel);
				gameOver = false;
				snakeLevel = 0;
				sVelX = 0.0f;
				sVelY = 0.0f;
				storyTime = 0.0f;
				snakePosX = (float)storyMapSize / 2;
				snakePosY = (float)storyMapSize / 2;
				GameState = GS::MAINMENU;
			}
			return true;
		}
		storyTime += fElapsedTime;
		if (storyTime > 150.0f) storyTime -= 150.0f;

		spawnTime += fElapsedTime;
		if (invincible > 0) invincible -= fElapsedTime;
		auto GetTile = [&](int x, int y)
		{
			if (x >= 0 && x < storyMapSize && y >= 0 && y < storyMapSize)
				return storyMap[x][y];
			else
				return -1;
		};

		auto SetTile = [&](int x, int y, int i)
		{
			if (x >= 0 && x < storyMapSize && y >= 0 && y < storyMapSize)
				storyMap[x][y] = i;
		};

		//Handle Input
		float preVelX = sVelX;
		float preVelY = sVelY;
		if (IsFocused())
		{
			if (velY == 0 && GetKey(olc::UP).bPressed || GetKey(olc::W).bPressed)
			{
				nDir = 0;
				sVelX = 0;
				sVelY = max(-10.0f, -2.0f - snakeLevel * 8.0f / 10.0f);
			}
			if (velY == 0 && GetKey(olc::DOWN).bPressed || GetKey(olc::S).bPressed)
			{
				nDir = 2;
				sVelX = 0;
				sVelY = min(10.0f, 2.0f + snakeLevel * 8.0f / 10.0f);
			}
			if (velX == 0 && GetKey(olc::LEFT).bPressed || GetKey(olc::A).bPressed)
			{
				nDir = 3;
				sVelX = max(-10.0f, -2.0f - snakeLevel * 8.0f / 10.0f);
				sVelY = 0;
			}
			if (velX == 0 && GetKey(olc::RIGHT).bPressed || GetKey(olc::D).bPressed)
			{
				nDir = 1;
				sVelX = min(10.0f, 2.0f + snakeLevel * 8.0f / 10.0f);
				sVelY = 0;
			}
			if (GetKey(olc::H).bPressed)
			{
				DrawString(300, 300, "Rabbit lv4", 2);
				DrawString(300, 400, "Bear lv 8", 2);
				DrawString(300, 500, "Immortal lv ???", 3);
				pause = (pause + 1) % 2;
			}
			if (GetKey(olc::F1).bPressed)
			{
				storyTime += fElapsedTime;
				snakeLevel++;
			}
			if (GetKey(olc::ESCAPE).bPressed || GetKey(olc::P).bPressed)
			{
				saveStory();
				resetSnake();
				GameState = GS::MAINMENU;
			}
		}
		if (pause) return true;
		float newSnakePosX = snakePosX + sVelX * fElapsedTime;
		float newSnakePosY = snakePosY + sVelY * fElapsedTime;

		while (!listSpawns.empty() && spawnTime > 2.0f)
		{
			spawnTime -= 2.0f;
			animal a;
			a.def = listSpawns.front();
			for (int d = 0; d < listSpawners.size(); d++)
				if (listSpawners[d].spawnType == a.def.strength)
				{
					a.pos = listSpawners[d].pos;
					auto it = listSpawners.begin() + d;
					rotate(it, it + 1, listSpawners.end());
				}
			listSpawns.pop_front();
			listAnimals.push_back(a);
		}

		while (!listImmDef.empty() && storyTime > 120.0f)
		{
			animal a;
			a.def = listImmDef.front();
			switch (rand() % 4)
			{
			case 0:
				a.pos = { 45.0f, 45.0f };
				break;
			case 1:
				a.pos = { 45.0f, 55.0f };
				break;
			case 2:
				a.pos = { 55.0f, 45.0f };
				break;
			case 3:
				a.pos = { 55.0f, 55.0f };
				break;
			}
			listImmDef.pop_front();
			listImmortals.push_back(a);
		}

		//Update animal
		for (auto& a : listAnimals)
		{
			a.Update(fElapsedTime);
			if (snakeLevel >= a.def.strength && (a.pos + olc::vf2d(1.0f, 1.0f) - olc::vf2d(snakePosX + 0.5f, snakePosY + 0.5f)).mag2() < 1.5f)
			{
				animalDefinition d = a.def;
				listSpawns.push_back(d);
				a.def.dead = true;
				length += a.def.strength * 2;
			}
			else if (snakeLevel < a.def.strength && (a.pos - olc::vf2d(snakePosX, snakePosY)).mag2() < 1.5f && invincible <= 0.0f)
			{
				if (length == 0) gameOver = true;
				invincible = 2.0f;
				length -= a.def.strength * 2;
				if (length < 0) length = 0;
			}
		}

		for (auto& i : listImmortals)
		{
			i.Update(fElapsedTime);
			if (storyTime < 120.0f)
			{
				animalDefinition d = i.def;
				listImmDef.push_back(d);
				i.def.dead = true;
			}
			else if ((i.pos - olc::vf2d(snakePosX, snakePosY)).mag2() < 1.5f && invincible <= 0.0f)
			{
				if (length == 0) gameOver = true;
				invincible = 2.0f;
				length = 0;
			}
		}

		for (auto& f : listFruits)
		{
			if ((f.pos - olc::vf2d(snakePosX, snakePosY)).mag2() < 1.5f && f.avail)
			{
				length++;
				f.avail = false;
				f.regenTime = 20.0f;
			}
			if (f.regenTime > 0)
			{
				f.regenTime -= fElapsedTime;
				if (f.regenTime <= 0)
					f.avail = true;
			}
		}

		if (length >= snakeLevel * 10)
		{
			length -= snakeLevel * 10;
			snakeLevel++;
		}

		//Remove animal
		listAnimals.remove_if([&](const animal& a) { return a.def.dead; });
		listImmortals.remove_if([&](const animal& a) { return a.def.dead; });

		//Remove fruit



		//Collision
		if (sVelX < 0)
		{
			if (GetTile((int)newSnakePosX, (int)snakePosY) == 's' || GetTile((int)newSnakePosX, int(snakePosY + 0.999f)) == 's')
			{
				newSnakePosX = (int)newSnakePosX + 1.0f;
				sVelX = preVelX;
				sVelY = preVelY;
			}
		}
		else if (sVelX > 0)
		{
			if (GetTile(int(newSnakePosX + 1.0f), (int)snakePosY) == 's' || GetTile(int(newSnakePosX + 1.0f), int(snakePosY + 0.999f)) == 's')
			{
				newSnakePosX = (int)newSnakePosX + 0.0f;
				sVelX = preVelX;
				sVelY = preVelY;
			}
		}

		if (sVelY < 0)
		{
			if (GetTile((int)newSnakePosX, (int)newSnakePosY) == 's' || GetTile(int(newSnakePosX + 0.999f), (int)newSnakePosY) == 's')
			{
				newSnakePosY = (int)newSnakePosY + 1.0f;
				sVelX = preVelX;
				sVelY = preVelY;
			}
		}
		else if (sVelY > 0)
		{
			if (GetTile((int)newSnakePosX, int(snakePosY + 1.0f)) == 's' || GetTile(int(newSnakePosX + 0.999f), int(snakePosY + 1.0f)) == 's')
			{
				newSnakePosY = (int)newSnakePosY + 0.0f;
				sVelX = preVelX;
				sVelY = preVelY;
			}
		}

		if (newSnakePosX < 0.0f) newSnakePosX = 0.0f;
		if (newSnakePosX >= storyMapSize - 1) newSnakePosX = (float)storyMapSize - 1;
		if (newSnakePosY < 0.0f) newSnakePosY = 0.0f;
		if (newSnakePosY >= storyMapSize - 1) newSnakePosY = (float)storyMapSize - 1;




		snakePosX = newSnakePosX;
		snakePosY = newSnakePosY;

		cameraPosX = snakePosX;
		cameraPosY = snakePosY;

		//Draw map
		int visibleX = ScreenWidth() / tileSize;
		int visibleY = ScreenHeight() / tileSize;
		float offsetX = cameraPosX - (float)visibleX / 2;
		float offsetY = cameraPosY - (float)visibleY / 2;
		if (offsetX < 0) offsetX = 0;
		if (offsetY < 0) offsetY = 0;
		if (offsetX > storyMapSize - visibleX) offsetX = (float)storyMapSize - visibleX;
		if (offsetY > storyMapSize - visibleY) offsetY = (float)storyMapSize - visibleY;

		float tileOffsetX = (offsetX - (int)offsetX) * tileSize;
		float tileOffsetY = (offsetY - (int)offsetY) * tileSize;

		Clear(olc::VERY_DARK_GREEN);
		for (int x = -4; x < visibleX + 4; x++)
		{
			for (int y = -4; y < visibleY + 4; y++)
			{
				int tileID = GetTile(x + (int)offsetX, y + (int)offsetY);
				switch (tileID)
				{
				case 0:		// blank tile
					//FillRect(x * tileSize - tileOffsetX, y * tileSize - tileOffsetY, tileSize, tileSize, olc::VERY_DARK_GREEN);
					//DrawDecal({ x * tileSize - tileOffsetX, y * tileSize - tileOffsetY }, grassDecal);
					DrawRect(int(x * tileSize - tileOffsetX), int(y * tileSize - tileOffsetY), tileSize, tileSize, olc::VERY_DARK_BLUE);
					break;
				case 'h':	// house
					//FillRect(x * tileSize - tileOffsetX, y * tileSize - tileOffsetY, tileSize, tileSize, olc::GREY);
					DrawPartialDecal({ x * tileSize - tileOffsetX, y * tileSize - tileOffsetY }, houseDecal, { 0.0f, 0.0f }, { tileSize * 1.5f, tileSize * 1.5f }, { 2.0f,2.0f });
					break;
				case 't':	// tree
					//FillRect(x * tileSize - tileOffsetX, y * tileSize - tileOffsetY, tileSize, tileSize, olc::VERY_DARK_CYAN);
					DrawDecal({ x * tileSize - tileOffsetX, y * tileSize - tileOffsetY }, treeDecal);
					break;
				case 'b':
					//DrawPartialDecal({ x * tileSize - tileOffsetX, y * tileSize - tileOffsetY }, animalDecal[0], { 1.0f * tileSize, 0.0f * tileSize }, { (float)tileSize, (float)tileSize });
					DrawDecal({ x * tileSize - tileOffsetX, y * tileSize - tileOffsetY }, decSpawner[1]);
					break;
				case 'c':
					DrawDecal({ x * tileSize - tileOffsetX, y * tileSize - tileOffsetY }, decSpawner[0]);
					break;
				default:
					break;
				}
			}
		}
		//Draw animal
		//FillRect((snakePosX - offsetX) * tileSize, (snakePosY - offsetY) * tileSize, tileSize, tileSize, olc::GREEN);
		for (auto& f : listFruits)
			if (f.avail) DrawDecal({ (f.pos.x - offsetX + 0.25f) * tileSize, (f.pos.y - offsetY + 0.25f) * tileSize }, fruitDecal);
		for (auto& a : listAnimals)
			DrawDecal({ (a.pos.x - offsetX) * tileSize, (a.pos.y - offsetY) * tileSize }, animalDecal[a.def.spriteID]);
		for (auto& i : listImmortals)
			DrawDecal({ (i.pos.x - offsetX) * tileSize, (i.pos.y - offsetY) * tileSize }, animalDecal[i.def.spriteID]);

		float color = max(0.0f, 225.0f * (1.0f - snakeLevel / 10));
		if (GetTile(int(snakePosX + 0.5f), int(snakePosY + 0.5f)) == 0)
			DrawRotatedDecal({ (snakePosX - offsetX) * tileSize, (snakePosY - offsetY) * tileSize }, headDecal, 3.14159f / 2.0f * nDir,
				{ float(headSprite->width) / 2.0f, float(headSprite->height) / 2.0f },
				{ min(2.0f * snakeLevel / 5.0f, 2.5f), min(2.0f * snakeLevel / 5.0f, 2.5f) },
				invincible > 0 ? olc::Pixel(225, int(color), int(color), 64) : olc::Pixel(225, int(color), int(color)));

		if (snakePosX > 48.0f && snakePosX < 53.0f && snakePosY > 48.0f && snakePosY < 53.0f)
			DrawRect(int((48 - offsetX) * tileSize), int((48 - offsetY) * tileSize), 5 * 64, 5 * 64, olc::GREEN);

		float light = min(225.0f, storyTime * 225.0f / 120.0f);
		DrawDecal({ 0.0f, 0.0f }, decDawn, { 1.0f, 1.0f }, olc::Pixel(225, 225, 225, int(light)));

		//Draw HUD
		drawString(4, 4, "Level " + to_string(snakeLevel), "Cap: " + to_string(snakeLevel), olc::WHITE, 3);
		DrawRect(220, 8, 1000, 8, olc::WHITE);
		FillRect(220, 8, int((float)length / (snakeLevel * 10.0f) * 1000.0f), 8, olc::WHITE);

		return true;
	}

	bool Classic(float fElapsedTime)
	{
		if (IsFocused())
		{
			if ((velY == 0 && GetKey(olc::UP).bPressed || GetKey(olc::W).bPressed) && !pause)
			{
				velX = 0;
				velY = -1;
			}
			if ((velY == 0 && GetKey(olc::DOWN).bPressed || GetKey(olc::S).bPressed) && !pause)
			{
				velX = 0;
				velY = 1;
			}
			if ((velX == 0 && GetKey(olc::LEFT).bPressed || GetKey(olc::A).bPressed) && !pause)
			{
				velX = -1;
				velY = 0;
			}
			if ((velX == 0 && GetKey(olc::RIGHT).bPressed || GetKey(olc::D).bPressed) && !pause)
			{
				velX = 1;
				velY = 0;
			}
			if (GetKey(olc::ESCAPE).bPressed || GetKey(olc::P).bPressed)
			{
				pause = !pause;
			}
			if (pause)
			{
				drawString(350, 250, "EXIT and SAVE?", "THOAT va LUU?", olc::WHITE, 5);
				drawString(450, 350, "YES", "CO", exit == true ? olc::YELLOW : olc::WHITE, 5);
				drawString(650, 350, "NO", "KHONG", exit == false ? olc::YELLOW : olc::WHITE, 5);
				if (GetKey(olc::LEFT).bPressed || GetKey(olc::A).bPressed || GetKey(olc::RIGHT).bPressed || GetKey(olc::D).bPressed)
				{
					playSound(over);
					exit = !exit;
				}
				if (GetKey(olc::ENTER).bPressed || GetKey(olc::SPACE).bPressed)
				{
					playSound(click);
					if (exit)
					{
						gameOver = true;
						lost_save = false;
						saveGame();
					}
					pause = !pause;
				}
				return 1;
			}
			else
				time += fElapsedTime;
		}

		if (time >= speed && (velX || velY))
		{
			Clear(olc::BLANK);
			for (auto i = length - 1; i > 0; i--)
			{
				auto prev = i - 1;
				segments[i].first = segments[prev].first;
				segments[i].second = segments[prev].second;
			}
			if (length)
			{
				segments[0].first = headPosX;
				segments[0].second = headPosY;
			}
			pair<int, int> prePos = make_pair(headPosX, headPosY);
			headPosX += velX;
			headPosY += velY;

			if (headPosY >= mapHeight)	headPosY = 0;
			if (headPosY < 0)			headPosY = (mapHeight - 1);
			if (headPosX >= mapWidth)	headPosX = 0;
			if (headPosX < 0)			headPosX = (mapWidth - 1);

			// Snake eat the fruit
			if (headPosX == fruitPosX && headPosY == fruitPosY)
			{
				score++;
				length++;
				fruitPosX = rand() % mapWidth;
				fruitPosY = rand() % mapHeight;
				segments.push_back(prePos);
				playSound(eat);
			}
			for (int i = 0; i < length; i++)
			{
				if (headPosX == segments[i].first && headPosY == segments[i].second)
					gameOver = true;
			}

			drawString(screenPosY, screenPosY, "Score: " + to_string(length), "Diem: " + to_string(length), olc::WHITE, 3);
			time = 0;
		}

		// Draw
		DrawDecal({ (float)fruitPosX * tileSize + screenPosX, (float)fruitPosY * tileSize + screenPosY }, fruitDecal);
		DrawDecal({ (float)headPosX * tileSize + screenPosX, (float)headPosY * tileSize + screenPosY }, headDecal);
		for (int i = 0; i < length; i++)
		{
			DrawDecal({ (float)segments[i].first * tileSize + screenPosX, (float)segments[i].second * tileSize + screenPosY }, bodyDecal);
		}

		if (gameOver)
		{
			playSound(die);
			gameOver = false;
			if (lost_save == true)
			{
				saveHighscore(score);
				remove("GameSave.txt");
			}
			GameState = GS::MAINMENU;
			resetSnake();
			Clear(olc::BLANK);
		}
		return true;
	}

	bool Modern(float fElapsedTime)
	{
		if (IsFocused()) {
			if ((velY == 0 && GetKey(olc::UP).bPressed || GetKey(olc::W).bPressed) && !pause)
			{
				velX = 0;
				velY = -1;
			}
			if ((velY == 0 && GetKey(olc::DOWN).bPressed || GetKey(olc::S).bPressed) && !pause)
			{
				velX = 0;
				velY = 1;
			}
			if ((velX == 0 && GetKey(olc::LEFT).bPressed || GetKey(olc::A).bPressed) && !pause)
			{
				velX = -1;
				velY = 0;
			}
			if ((velX == 0 && GetKey(olc::RIGHT).bPressed || GetKey(olc::D).bPressed) && !pause)
			{
				velX = 1;
				velY = 0;
			}
			if (GetKey(olc::ESCAPE).bPressed || GetKey(olc::P).bPressed)
			{
				pause = !pause;
			}
			if (pause)
			{
				drawString(350, 250, "EXIT and SAVE?", "THOAT va LUU?", olc::WHITE, 5);
				drawString(450, 350, "YES", "CO", exit == true ? olc::YELLOW : olc::WHITE, 5);
				drawString(650, 350, "NO", "KHONG", exit == false ? olc::YELLOW : olc::WHITE, 5);
				if (GetKey(olc::LEFT).bPressed || GetKey(olc::A).bPressed || GetKey(olc::RIGHT).bPressed || GetKey(olc::D).bPressed)
				{
					playSound(over);
					exit = !exit;
				}
				if (GetKey(olc::ENTER).bPressed || GetKey(olc::SPACE).bPressed)
				{
					playSound(click);
					if (exit)
					{
						gameOver = true;
						lost_save = false;
						saveGame();
					}
					pause = !pause;
				}
				return 1;
			}
			else
				time += fElapsedTime;
		}
		if (time >= speed && (velX || velY))
		{
			Clear(olc::BLANK);
			for (auto i = length - 1; i > 0; i--)
			{
				auto prev = i - 1;
				segments[i].first = segments[prev].first;
				segments[i].second = segments[prev].second;
			}
			if (length)
			{
				segments[0].first = headPosX;
				segments[0].second = headPosY;
			}
			pair<int, int> prePos = make_pair(headPosX, headPosY);
			headPosX += velX;
			headPosY += velY;
			// if snake head collide with walls, game over
			if (headPosX >= mapWidth || headPosX < 0 || headPosY >= mapHeight || headPosY < 0) {
				gameOver = true;
			}
			// Snake eat the fruit
			if (headPosX == fruitPosX && headPosY == fruitPosY)
			{
				score = score + 1;
				length++;
				fruitPosX = rand() % mapWidth;
				fruitPosY = rand() % mapHeight;
				segments.push_back(prePos);
				playSound(eat);
			}
			// if snake head collide with its body, game over
			for (int i = 0; i < length; i++) {
				if (headPosX == segments[i].first && headPosY == segments[i].second) {
					gameOver = true;
				}
			}

			drawString(screenPosY, screenPosY, "Score: " + to_string(length), "Diem: " + to_string(length), olc::WHITE, 3);
			time = 0;
		}

		// Draw
		for (int i = -1; i < mapHeight + 1; i++)
		{
			DrawDecal({ (float)screenPosX - tileSize, (float)screenPosY + i * tileSize }, wallDecal);
			DrawDecal({ (float)screenPosX + mapWidth * tileSize, (float)screenPosY + i * tileSize }, wallDecal);
			DrawDecal({ (float)screenPosX + i * tileSize, (float)screenPosY - tileSize }, wallDecal);
			DrawDecal({ (float)screenPosX + i * tileSize, (float)screenPosY + mapHeight * tileSize }, wallDecal);
		}

		DrawDecal({ (float)fruitPosX * tileSize + screenPosX, (float)fruitPosY * tileSize + screenPosY }, fruitDecal);
		DrawDecal({ (float)headPosX * tileSize + screenPosX, (float)headPosY * tileSize + screenPosY }, headDecal);
		for (int i = 0; i < length; i++)
		{
			DrawDecal({ (float)segments[i].first * tileSize + screenPosX, (float)segments[i].second * tileSize + screenPosY }, bodyDecal);
		}

		if (gameOver)
		{
			playSound(die);
			gameOver = false;
			if (lost_save == true)
			{
				saveHighscore(score);
				remove("GameSave.txt");
			}
			GameState = GS::MAINMENU;
			resetSnake();
			Clear(olc::BLANK);
		}
		return true;
	}

	bool Night(float fElapsedTime)
	{
		if (IsFocused())
		{
			if ((velY == 0 && GetKey(olc::UP).bPressed || GetKey(olc::W).bPressed) && !pause)
			{
				velX = 0;
				velY = -1;
			}
			if ((velY == 0 && GetKey(olc::DOWN).bPressed || GetKey(olc::S).bPressed) && !pause)
			{
				velX = 0;
				velY = 1;
			}
			if ((velX == 0 && GetKey(olc::LEFT).bPressed || GetKey(olc::A).bPressed) && !pause)
			{
				velX = -1;
				velY = 0;
			}
			if ((velX == 0 && GetKey(olc::RIGHT).bPressed || GetKey(olc::D).bPressed) && !pause)
			{
				velX = 1;
				velY = 0;
			}
			if (GetKey(olc::ESCAPE).bPressed || GetKey(olc::P).bPressed)
			{
				pause = !pause;
			}
			if (pause)
			{
				drawString(350, 250, "EXIT and SAVE?", "THOAT va LUU?", olc::WHITE, 5);
				drawString(450, 350, "YES", "CO", exit == true ? olc::YELLOW : olc::WHITE, 5);
				drawString(650, 350, "NO", "KHONG", exit == false ? olc::YELLOW : olc::WHITE, 5);
				if (GetKey(olc::LEFT).bPressed || GetKey(olc::A).bPressed || GetKey(olc::RIGHT).bPressed || GetKey(olc::D).bPressed)
				{
					playSound(over);
					exit = !exit;
				}
				if (GetKey(olc::ENTER).bPressed || GetKey(olc::SPACE).bPressed)
				{
					playSound(click);
					if (exit)
					{
						gameOver = true;
						lost_save = false;
						saveGame();
					}
					pause = !pause;
				}
				return 1;
			}
			else
				time += fElapsedTime;
		}

		if (time >= speed && (velX || velY))
		{
			Clear(olc::BLANK);
			for (auto i = length - 1; i > 0; i--)
			{
				auto prev = i - 1;
				segments[i].first = segments[prev].first;
				segments[i].second = segments[prev].second;
			}
			if (length)
			{
				segments[0].first = headPosX;
				segments[0].second = headPosY;
			}
			pair<int, int> prePos = make_pair(headPosX, headPosY);
			headPosX += velX;
			headPosY += velY;

			if (headPosY >= mapHeight) headPosY = 0;
			if (headPosY < 0) headPosY = (mapHeight - 1);
			if (headPosX >= mapWidth) headPosX = 0;
			if (headPosX < 0) headPosX = (mapWidth - 1);

			// Snake eat the fruit
			if (headPosX == fruitPosX && headPosY == fruitPosY)
			{
				score = score + 1;
				length++;
				fruitPosX = rand() % mapWidth;
				fruitPosY = rand() % mapHeight;
				segments.push_back(prePos);
				playSound(eat);
			}
			for (int i = 0; i < length; i++)
			{
				if (headPosX == segments[i].first && headPosY == segments[i].second)
					gameOver = true;
			}

			drawString(screenPosY, screenPosY, "Score: " + to_string(length), "Diem: " + to_string(length), olc::WHITE, 3);
			time = 0;
		}

		// Draw map
		DrawPartialDecal({ (float)screenPosX, (float)screenPosY }, nightDecal, { 0.0f, 0.0f }, { (float)mapWidth * tileSize, max((float)(headPosY - 2) * tileSize, 0.0f) });
		DrawPartialDecal({ (float)screenPosX, (float)screenPosY + (headPosY + 3) * tileSize }, nightDecal, { 0.0f, (float)(headPosY + 3) * tileSize }, { (float)mapWidth * tileSize, max((float)(mapHeight - headPosY - 3) * tileSize, 0.0f) });
		DrawPartialDecal({ (float)screenPosX, (float)screenPosY }, nightDecal, { 0.0f, 0.0f }, { max((float)(headPosX - 2) * tileSize, 0.0f), (float)mapHeight * tileSize });
		DrawPartialDecal({ (float)screenPosX + (headPosX + 3) * tileSize, (float)screenPosY }, nightDecal, { (float)(headPosX + 3) * tileSize, 0.0f }, { max((float)(mapWidth - headPosX - 3) * tileSize, 0.0f), (float)mapHeight * tileSize });

		if (abs(fruitPosX - headPosX) <= 2 && abs(fruitPosY - headPosY) <= 2)
			DrawDecal({ (float)fruitPosX * tileSize + screenPosX, (float)fruitPosY * tileSize + screenPosY }, fruitDecal);

		// Draw Snake
		DrawDecal({ (float)headPosX * tileSize + screenPosX, (float)headPosY * tileSize + screenPosY }, headDecal);
		for (int i = 0; i < length; i++)
		{
			if (abs(segments[i].first - headPosX) <= 2 && abs(segments[i].second - headPosY) <= 2)
				DrawDecal({ (float)segments[i].first * tileSize + screenPosX, (float)segments[i].second * tileSize + screenPosY }, bodyDecal);
		}

		if (gameOver)
		{
			playSound(die);
			gameOver = false;
			if (lost_save == true)
			{
				saveHighscore(score);
				remove("GameSave.txt");
			}
			GameState = GS::MAINMENU;
			resetSnake();
			Clear(olc::BLANK);
		}
		return true;
	}

	int settingsSelection = 0;
	bool Settings()
	{
		Clear(olc::BLANK);
		int nSpeed = 11 - int(10.0 * speed / 0.5f);
		cout << speed << " " << nSpeed << endl;
		drawString(350, 50, "Settings", "Cai dat", olc::WHITE, 5);
		drawString(350, 200, "Speed: " + to_string(nSpeed), "Toc do: " + to_string(nSpeed), settingsSelection == 0 ? olc::YELLOW : olc::WHITE, 5);
		drawString(350, 300, sound == true ? "Sound: ON" : "Sound: OFF", sound == true ? "Am thanh: BAT" : "Am thanh: TAT", settingsSelection == 1 ? olc::YELLOW : olc::WHITE, 5);
		drawString(350, 400, music == true ? "Music: ON" : "Music: OFF", music == true ? "Nhac: BAT" : "Nhac: TAT", settingsSelection == 2 ? olc::YELLOW : olc::WHITE, 5);
		drawString(350, 500, "Language: English", "Ngon ngu: Tieng Viet", settingsSelection == 3 ? olc::YELLOW : olc::WHITE, 5);
		drawString(350, 600, "Back", "Tro lai", settingsSelection == 4 ? olc::YELLOW : olc::WHITE, 5);
		if (GetKey(olc::UP).bPressed || GetKey(olc::W).bPressed)
		{
			playSound(over);
			settingsSelection--;
			if (settingsSelection < 0) settingsSelection = 4;
		}
		if (GetKey(olc::DOWN).bPressed || GetKey(olc::S).bPressed)
		{
			playSound(over);
			settingsSelection++;
			if (settingsSelection > 4) settingsSelection = 0;
		}
		if (GetKey(olc::LEFT).bPressed || GetKey(olc::A).bPressed)
		{
			playSound(click);
			if (settingsSelection == 0 && speed < 0.5f) speed += 0.05f;
			if (settingsSelection == 1) sound = !sound;
			if (settingsSelection == 2) {
				//music = !music;
				playMusic(faded);
			}
			if (settingsSelection == 3) language = !language;
		}
		if (GetKey(olc::RIGHT).bPressed || GetKey(olc::D).bPressed)
		{
			playSound(click);
			if (settingsSelection == 0 && speed > 0.06f) speed -= 0.05f;
			if (settingsSelection == 1) sound = !sound;
			if (settingsSelection == 2) {
				//music = !music;
				playMusic(faded);
			}
			if (settingsSelection == 3) language = !language;
		}
		if (GetKey(olc::SPACE).bPressed || GetKey(olc::ENTER).bPressed)
		{
			playSound(click);
			if (settingsSelection == 1) sound = !sound;
			if (settingsSelection == 2) {
				//music = !music;
				playMusic(faded);
			}
			if (settingsSelection == 3) language = !language;
			if (settingsSelection == 4) GameState = GS::MAINMENU;
			saveSettings();
			//Clear(olc::BLANK);
		}
		return true;
	}

	int loadGame()
	{
		ifstream fileInput("GameSave.txt");
		int n, res = 0;
		if (fileInput.fail())
		{
			fileInput.close();
			return res;
		}
		fileInput >> n;
		if (fileInput.eof())
		{
			fileInput.close();
			return res;
		}
		res = n;
		fileInput >> n;
		fruitPosX = n / 100;
		fruitPosY = n % 100;
		if (GameState == GS::STORY) fileInput >> score >> length;
		else fileInput >> score >> length;
		fileInput >> n;
		/*switch (n)
		{
		case -1:
			velX = 0;
			velY = 0;
			break;
		case 0:
			velX = 0;
			velY = -1;
			break;
		case 1:
			velX = 1;
			velY = 0;
			break;
		case 2:
			velX = 0;
			velY = 1;
			break;
		case 3:
			velX = -1;
			velY = 0;
			break;
		default:
			break;
		}*/
		fileInput >> n;
		headPosX = n / 100;
		headPosY = n % 100;
		for (int i = 0; i < score; i++)
		{
			fileInput >> n;
			segments.push_back(make_pair(n / 100, n % 100));
		}
		fileInput.close();
		return res;
	}

	bool loadStory()
	{
		ifstream fileInput("StorySave.txt");
		if (fileInput.fail())
		{
			fileInput.close();
			return 0;
		}
		fileInput >> snakeLevel;
		if (fileInput.eof())
		{
			fileInput.close();
			return 0;
		}
		listSpawns.clear();
		listAnimals.clear();
		listImmDef.clear();
		listImmortals.clear();
		listFruits.clear();
		listSpawners.clear();
		fileInput.close();
		return 1;
	}

	bool saveGame()
	{
		ofstream fileOutput("GameSave.txt");
		if (fileOutput.fail())
		{
			fileOutput.close();
			return 0;
		}
		switch (GameState)
		{
		case GS::CLASSIC:
			fileOutput << 1;
			break;
		case GS::MODERN:
			fileOutput << 2;
			break;
		case GS::NIGHT:
			fileOutput << 3;
			break;
		default:
			break;
		}
		fileOutput << ' ';
		if (fruitPosX < 10) fileOutput << '0' << fruitPosX;
		else fileOutput << fruitPosX;
		if (fruitPosY < 10) fileOutput << '0' << fruitPosY;
		else fileOutput << fruitPosY;
		fileOutput << ' ' << score << ' ' << length << ' ';
		// 0 for UP, 1 for RIGHT, 2 for DOWN, 3 for LEFT, -1 for STAY
		if (velX == 0 && velY == -1) fileOutput << '0' << ' ';
		else if (velX == 1 && velY == 0) fileOutput << '1' << ' ';
		else if (velX == 0 && velY == 1) fileOutput << '2' << ' ';
		else if (velX == -1 && velY == 0) fileOutput << '3' << ' ';
		else fileOutput << '-1' << ' ';
		if (headPosX < 10) fileOutput << '0' << headPosX;
		else fileOutput << headPosX;
		if (headPosY < 10) fileOutput << '0' << headPosY;
		else fileOutput << headPosY;
		for (int i = 0; i < score; i++)
		{
			fileOutput << ' ';
			if (segments[i].first < 10)
			{
				fileOutput << '0' << segments[i].first;
			}
			else
			{
				fileOutput << segments[i].first;
			}
			if (segments[i].second < 10)
			{
				fileOutput << '0' << segments[i].second;
			}
			else
			{
				fileOutput << segments[i].second;
			}
		}
		fileOutput.close();
		return 1;
	}

	bool saveStory()
	{
		ofstream fileOutput("StorySave.txt");
		if (fileOutput.fail())
		{
			fileOutput.close();
			return 0;
		}
		fileOutput << snakeLevel;
		fileOutput.close();
		return 1;
	}

	bool loadSettings()
	{
		ifstream fileInput("Settings.txt");
		if (fileInput.fail())
		{
			fileInput.close();
			return 0;
		}
		fileInput >> speed >> language >> sound >> music;
		return 1;
	}

	bool saveSettings()
	{
		ofstream fileOutput("Settings.txt");
		if (fileOutput.fail())
		{
			fileOutput.close();
			return 0;
		}
		fileOutput << speed << ' ' << language << ' ' << sound << ' ' << music;
		return 1;
	}

	bool loadHighscore()
	{
		ifstream fileInput("Highscore.txt");
		if (fileInput.fail())
		{
			fileInput.close();
			GameState = GS::MAINMENU;
			return 0;
		}
		string s;
		for (int i = 0; getline(fileInput, s); i++)
		{
			istringstream ss(s);
			ss >> s >> highscore[i];
		}
		fileInput.close();
		return 1;
	}

	bool saveHighscore(int score)
	{
		ifstream fileInput("Highscore.txt");
		if (fileInput.fail())
		{
			fileInput.close();
			GameState = GS::MAINMENU;
			return 0;
		}
		string s;
		if (GameState == GS::STORY && score > highscore[0]) highscore[0] = score;
		if (GameState == GS::CLASSIC && score > highscore[1]) highscore[1] = score;
		if (GameState == GS::MODERN && score > highscore[2]) highscore[2] = score;
		if (GameState == GS::NIGHT && score > highscore[3]) highscore[3] = score;
		ofstream fileOutput("Highscore.txt");
		fileOutput << "Story " << highscore[0] << '\n';
		fileOutput << "Classic " << highscore[1] << '\n';
		fileOutput << "Modern " << highscore[2] << '\n';
		fileOutput << "Night " << highscore[3];
		fileOutput.close();
		return 1;
	}

	bool checkToCreateHighscore()
	{
		ifstream fileInput("Highscore.txt");
		if (fileInput.fail())
		{
			fileInput.close();
			ofstream fileOutput("Highscore.txt");
			fileOutput << "Story 0\n";
			fileOutput << "Classic 0\n";
			fileOutput << "Modern 0\n";
			fileOutput << "Night 0";
			fileOutput.close();
			for (int i = 0; i < 4; i++) highscore.push_back(0);
			return 1;
		}
		int n;
		fileInput >> n;
		if (fileInput.eof())
		{
			fileInput.close();
			ofstream fileOutput("Highscore.txt");
			fileOutput << "Story 0\n";
			fileOutput << "Classic 0\n";
			fileOutput << "Modern 0\n";
			fileOutput << "Night 0";
			fileOutput.close();
			for (int i = 0; i < 4; i++) highscore.push_back(0);
		}
		else fileInput.close();
		return 1;
	}

	// New functions go up here

	bool OnUserUpdate(float fElapsedTime) override
	{
		EnableLayer(layerMap, false);
		switch (GameState)
		{
		case GS::MAINMENU:
			MainMenu();
			break;
		case GS::PLAYGAME:
			PlayGame();
			break;
		case GS::CONTINUE:
			ContinueGame();
			break;
		case GS::HIGHSCORE:
			Highscore();
			break;
		case GS::STORY:
			tileSize = 64;
			Story(fElapsedTime);
			tileSize = 32;
			break;
		case GS::CLASSIC:
			EnableLayer(layerMap, true);
			Classic(fElapsedTime);
			break;
		case GS::MODERN:
			EnableLayer(layerMap, true);
			Modern(fElapsedTime);
			break;
		case GS::NIGHT:
			EnableLayer(layerMap, true);
			Night(fElapsedTime);
			break;
		case GS::TWOPLAYERS:
			EnableLayer(layerMap, true);
			TwoPlayers(fElapsedTime);
			break;
		case GS::SETTINGS:
			Settings();
			break;
		}
		return true;
	}

	bool OnUserDestroy() override
	{
		olc::SOUND::DestroyAudio();
		return true;
	}

	// End of Snake, do not insert new functions here
};

int main()
{
	Snake game;
	if (game.Construct(1280, 720, 1, 1))
		game.Start();
	return 0;
}